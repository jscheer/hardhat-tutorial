const { loadFixture } = require("@nomicfoundation/hardhat-network-helpers");
const { expect } = require("chai");

describe("FiebsToken contract", function () {

  // Deployment fixtures
  async function deployTokenFixture() {

    const [owner, account1, account2] = await ethers.getSigners();
    const Token = await ethers.getContractFactory("FiebsToken");

    const hardhatToken = await Token.deploy();
    await hardhatToken.deployed();

    // Fixtures can return anything you consider useful for your tests
    return { Token, hardhatToken, owner, account1, account2 };
  }

  describe("Deployment", function () {

    it("Deployment should set right owner", async function () {

      const { hardhatToken, owner } = await loadFixture(deployTokenFixture);

      expect(await hardhatToken.owner()).to.equal(owner.address);
    });

    it("Deployment should assign the total supply of tokens to the owner", async function () {

      const { hardhatToken, owner } = await loadFixture(deployTokenFixture);

      const totalSupply = await hardhatToken.totalSupply();
      ownerBalance = await hardhatToken.balanceOf(owner.address);

      expect(totalSupply).to.equal(ownerBalance);
    });

    
    it("Deployment should keep other accounts empty.", async function () {

      const { hardhatToken, owner, account1, account2 } = await loadFixture(deployTokenFixture);

      // Check initial balances of account1 and account2
      expect(await hardhatToken.balanceOf(account1.address)).to.equal(0);
      expect(await hardhatToken.balanceOf(account2.address)).to.equal(0);
    });
  });

  describe("Transfer", function () {

    it("Should transfer Tokens between accounts", async function () {

      const { hardhatToken, owner, account1, account2 } = await loadFixture(deployTokenFixture);

      // Transfer 50 token from owner to account1
      await expect(
        hardhatToken.transfer(account1.address, 50)
      ).to.changeTokenBalances(hardhatToken, [owner, account1], [-50, 50]);

      // Transfer 10 tokens from account1 to account2
      await expect(
        hardhatToken.connect(account1).transfer(account2.address, 10)
      ).to.changeTokenBalances(hardhatToken, [account1, account2], [-10, 10]);

      // check against total supply
      ownerBalance = await hardhatToken.balanceOf(owner.address);
      acc1Balance = await hardhatToken.balanceOf(account1.address);
      acc2Balance = await hardhatToken.balanceOf(account2.address);
      const balanceSum = parseInt(ownerBalance) + parseInt(acc1Balance) + parseInt(acc2Balance);
      expect(await hardhatToken.totalSupply()).to.equal(balanceSum);

    });

    it("Should emit Transfer events", async function () {

      const { hardhatToken, owner, account1, account2 } = await loadFixture(deployTokenFixture);

      // Transfer 50 tokens from owner to account1
      await expect(hardhatToken.transfer(account1.address, 50))
        .to.emit(hardhatToken, "Transfer")
        .withArgs(owner.address, account1.address, 50);

      // Transfer 50 tokens from account1 to account2
      await expect(hardhatToken.connect(account1).transfer(account2.address, 10))
        .to.emit(hardhatToken, "Transfer")
        .withArgs(account1.address, account2.address, 10);

    });

    it("Should fail if sender doesn't have enough tokens", async function () {
      
      const { hardhatToken, owner, account1, account2 } = await loadFixture(deployTokenFixture);

      const initialOwnerBalance = await hardhatToken.balanceOf(owner.address);

      // Try to send 1 token from addr1 (0 tokens) to owner.
      await expect(
        hardhatToken.connect(account1).transfer(owner.address, 1)
      ).to.be.revertedWith("Not enough tokens");

      // Owner balance shouldn't have changed.
      expect(await hardhatToken.balanceOf(owner.address)).to.equal(
        initialOwnerBalance
      );
      
    });
  });
});