// This script transfers FIB token from the owner account to a target account on Görli
//
// The transfer() function of the FiebsToken contract at address
//   0xB46FFB451fB4bC2CBD2f555073B069426D9D3f39
// is called.
// (git: https://gitlab.com/jscheer/hardhat-tutorial/-/blob/main/contracts/FiebsToken.sol)
//
// Note: 
// Keys in hardhat.config.js must be set appropriatly. 
//
// author: Jonas Scheer
//
const hre = require("hardhat");

async function main() {

  const [deployer] = await ethers.getSigners();

  const targetAccount = "0xbeB3662C34C8be5172B32EF9D3012e5C7B8680c0";
  const contractAddress = "0xB46FFB451fB4bC2CBD2f555073B069426D9D3f39";

  console.log("");
  console.log("Owner account:", deployer.address);
  console.log("Target account:", targetAccount);
  
  // Get contract by address
  const contract = await hre.ethers.getContractAt("FiebsToken", contractAddress);

  // Get initaial balances
  console.log("\nInitial Balances: ");
  balanceTarget = await contract.balanceOf(targetAccount);
  balanceOwner = await contract.balanceOf(deployer.address);
  console.log("balanceOf(", targetAccount, ") = ", balanceTarget.toString());
  console.log("balanceOf(", deployer.address, ") = ", balanceOwner.toString());

  // Transfer FIB
  // (wait a few seconds and check target address: https://goerli.etherscan.io/address/0xbeB3662C34C8be5172B32EF9D3012e5C7B8680c0#tokentxns)
  await contract.transfer(targetAccount, 100);

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
