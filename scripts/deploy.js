// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// You can also run a script with `npx hardhat run <script>`. If you do that, Hardhat
// will compile your contracts, add the Hardhat Runtime Environment's members to the
// global scope, and execute the script.
const hre = require("hardhat");

async function main() {

  const [deployer] = await ethers.getSigners();

  console.log("Deploying contracts with the account:", deployer.address);
  console.log("ETH balance:", (await deployer.getBalance()).toString());

  const Token = await ethers.getContractFactory("FiebsToken");
  const token = await Token.deploy();

  await token.deployed();

  console.log("Token address:", token.address);
  console.log("FIEBS balance (owner):", (await token.balanceOf(deployer.address)).toString());
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
