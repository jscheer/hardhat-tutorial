require("@nomicfoundation/hardhat-toolbox");

// update keys for public deployments
const ALCHEMY_GOERLI_API_KEY     = "12345678901234567890123456789012345";
const GOERLI_PRIVATE_KEY  = "1234567890123456789012345678901234567890123456789012345678901234";
const ALCHEMY_MAINNET_API_KEY     = "12345678901234567890123456789012345";
const MAINNET_PRIVATE_KEY = "1234567890123456789012345678901234567890123456789012345678901234";

/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: "0.8.17",
  networks: {
    goerli: {
      url: `https://eth-goerli.g.alchemy.com/v2/${ALCHEMY_GOERLI_API_KEY}`,
      accounts: [GOERLI_PRIVATE_KEY]
    },
    mainnet: {
      url: `https://eth-mainnet.g.alchemy.com/v2/${ALCHEMY_MAINNET_API_KEY}`,
      accounts: [MAINNET_PRIVATE_KEY]
    }
  }
};
