# FIB (Fiebs) Token
The [FiebsToken](https://goerli.etherscan.io/address/0xb46ffb451fb4bc2cbd2f555073b069426d9d3f39) is a simple Token that was implemented by following the [**Hardhat Tutorial**](https://hardhat.org/tutorial).

## FIB Properties
- max supply: `1M`
- transfering up to `255 FIB` from one account to another. 
  - Only full FIB can be transferred (`uint8`).
- FIB does **not** implement the ERC-20 standard.

# Lessons Learned
Besides implementing:
- Smart Contract: [`FiebsToken.sol`](https://gitlab.com/jscheer/hardhat-tutorial/-/blob/main/contracts/FiebsToken.sol)
- Tests: [`FiebsToken.js`](https://gitlab.com/jscheer/hardhat-tutorial/-/blob/main/test/FiebsToken.js)
- Scripts: [`deploy.js`](https://gitlab.com/jscheer/hardhat-tutorial/-/blob/main/scripts/deploy.js) and [`transfer.js`](https://gitlab.com/jscheer/hardhat-tutorial/-/blob/main/scripts/transfer.js)

we used the below shell commands. 


## Init New Hardhat Project
### Create new Folder
``` bash
mkdir myProject
cd myProject
```

### Init Project with Yarn
Init Project:
``` bash
yarn init
```
Install Hardhat:
``` bash
yarn add --dev hardhat
```
run `hardhat`:
``` bash
npx hardhat
```

#### Test Smart Contract
``` bash
npx hardhat test
```

##### Check gas usage:
``` bash
REPORT_GAS=true npx hardhat test
```

#### Deploy Smart Contract
``` bash
npx hardhat run scripts/deploy.js
```